import tkinter as tk
import PySimpleGUI as sg
import psycopg2

connection_string = "host='localhost' dbname='prog22' user='postgres' password='5214'" # Connectie-string, vul hier je DB, username en password in.

# Voor dit voorbeeld heb je een tabel albums nodig met de juiste kolommen:
# CREATE TABLE albums ( albumId int primary key, album varchar not null, artist varchar not null, year int );

# Omdat ik hier de IDs hardcoded erin zet kan je niet dezelfde data meerdere keren toevoegen. Je kan de tabel weggooien en opnieuw aanmaken:
# DROP TABLE albums; CREATE TABLE albums ( albumId int primary key, album varchar not null, artist varchar not null, year int );

# Demo data voor inladen in PostgreSQL
albums = [ (1, "Empath", "Devin Townsend", 2019)
         , (2, "Spirituality and Distortion", "Igorrr", 2020)
         , (3, "The Rising", "Bruce Springsteen", 2002)
         , (4, "Misplaced Childhood", "Marillion", 1985)
         , (5, "The Wall", "Pink Floyd", 1979)
         , (6, "Rammstein", "Rammstein", 2019)
         , (7, "Endless Forms Most Beautiful", "Nightwish", 2017)
         , (8, "HVMAN. :II: NATVRE.", "Nightwish", 2017)
         , (9, "The Black Parade", "My Chemical Romance", 2006)
         ]

def postgres_insert():
    conn = psycopg2.connect(connection_string)
    with conn.cursor() as cursor:                  # <-- with / as om de cursor te openen en te sluiten, is veiliger dan `cursor = cursor.open` / `cursor.close()`
        for album in albums:
            print(album)
            cursor.execute("INSERT INTO albums (albumId, album, artist, year) VALUES (%s, %s, %s, %s);", album) # <-- %s om waardes in de query te zetten
                                                                                                                # dit is vergelijkbaar met f-strings maar
                                                                                                                # is in dit geval veiliger omdat psycopg2
                                                                                                                # SQL code eruit zal filteren.
        conn.commit()   # <-- Belangrijk om de aanpassingen permanent te makek.


def postgres_select():
    conn = psycopg2.connect(connection_string)
    with conn.cursor() as cursor:
        # Algemene query - haal alles uit de tabel
        cursor.execute("SELECT * FROM albums;")
        result = cursor.fetchall()                     # <-- nodig als ik iets uit de DB terug wil.
                                                       # Ook mogelijk: fetchone() of fetchmany(10).
                                                       # Dit kan nodig zijn om niet 1 miljoen resultaten
                                                       #  in een keer in je geheugen in te hoeven laden.

        # Print alle resultaten zoals Python ze binnenkrijgt
        print(result)

        # result is een gewone Python datastructuur, je kan dus loopen, waardes selecteren of rekenen
        # Voorbeeld: print alleen de derde kolom per resultaat
        for album in result:
            print(album[2])

        # Query met voorwaarden
        cursor.execute("SELECT artist, year FROM albums where year > 2010;")
        result = cursor.fetchall()
        print(result)

        # Voorwaarden meegeven uit Python (in dit geval: welke band wil ik de albums van zien).
        cursor.execute("SELECT album FROM albums where artist = %s;", ["Nightwish"])
        result = cursor.fetchall()
        print(result)


def caesar(text):
    """Codeer tekst met ROT13 Caesar Cipher. Werking niet relevant maar wordt gebruikt in GUI voorbeelden."""
    def shift(letter):
        c = ord(letter)
        if 65 <= c <= 90:
            offset = 65
        elif 97 <= c <= 122:
            offset = 97
        else:
            return letter
        return chr((c-offset + 13) % 26 + offset)

    return ''.join([shift(letter) for letter in text])


def tkinter_gui():
    window = tk.Tk()                                             # Maak een scherm
    instructions = tk.Label(text="Enter message below")          # Maak een label

    entry = tk.Entry()                                           # en een tekstvak

    def cipher_code():                                           # Functie binnen een functie - deze functie kan
        text = entry.get()                                       # bij entry om deze te lezen/schrijven.
        text = caesar(text)
        entry.delete(0, tk.END)                                  # Leeg het tekstvak
        entry.insert(0, text)                                    # Schrijf het resultaat van de codering in het tekstvak

    button = tk.Button(
        text="Casesar me!",
        width=25,
        height=5,
        command=cipher_code                                      # Koppel de functie aan de knop
    )

    instructions.pack()                                          # Voeg alle elementen toe aan het frame, op deze volgorde
    entry.pack()
    button.pack()

    window.mainloop()                                            # Start de GUI


# Alternatief: PySimpleGui
def psg_gui():
    sg.theme('BluePurple')

    layout = [[sg.Text('Enter message below'), sg.Text(size=(15, 1))],     # Geef de hele UI als een lijst, vergelijkbaar met HTML
              [sg.Input(key='MSG')],                                       # Tekstvak; MSG wordt gebruikt om de waarde te lezen / schrijven
              [sg.Button('Caesar me!')]]                                   # Knop; drukken geeft een event met de tekst "Caesar me!"

    window = sg.Window('PySimpleGUI', layout)                              # Maak een scherm met de gegeven layout

    event = 0                                                              # Initieel event, deze wordt in de loop geupdate. Gebruik geen None want dat is WIN_CLOSED

    while event != sg.WIN_CLOSED:                                          # Event loop, herhaal tot het scherm gesloten wordt
        event, values = window.read()                                      # Wacht op een event, lees het event en de waardes
        if event == 'Caesar me!':                                          # Als het event van de knop kwam
            text = values['MSG']                                           # Lees de waare van het tekstvak
            text = caesar(text)
            window['MSG'].update(text)                                     # Schrijf de waarde van het tekstvak


# Demo functionaliteit
if __name__ == "__main__":
    demos = {"1": psg_gui, "2": tkinter_gui, "3": postgres_insert, "4": postgres_select}

    print("1 PySimpleGUI")
    print("2 TKInter")
    print("3 PosgtreSQL INSERT")
    print("3 PosgtreSQL SELECT")

    keuze = input("Run een demo (1-4) ")

    if keuze in demos:
        demos[keuze]()
