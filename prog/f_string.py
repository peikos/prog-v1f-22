# Formatteren van tekst met f-string

import math

mijn_pizza = math.pi * 25 * 25

print(f"Mijn pizza-oppervlak is {mijn_pizza}!")      # Niet super leesbaar

print(f"Mijn pizza-oppervlak is {mijn_pizza:.2f}!")  # Beter

print(f"Ook een getal met minder cijfers krijgt er 2 achter de punt: {3:.2f}!")


# Uitlijnen van waardes uit een lijst

namen = ['Alice', 'Bob', 'Carol']

# Nu staat alles links onder elkaar
for naam in namen:
    print(naam)

# Nu staat alles rechts onder elkaar - iedere string wordt tot 8 karakters uitgebreid met spaties
for naam in namen:
    print(f"{naam: >8}")
