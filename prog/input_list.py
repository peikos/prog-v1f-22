# In een van de voorbeelden uit het boek wordt gevraagd een door de gebruiker gegeven lijst te filteren.
# De manier waarop je dit voor elkaar kan krijgen, zoals dit in het voorbeeld staat, is met `eval`, maar zoals gezegd
# is dit geen goede functie om jezelf aan te leren.
# Voor de oefening kan je de lijst uit het voorbeeld direct in de code zetten en de input step overslaan.

# Als je toch graag een lijst van de gebruiker wil accepteren, dan is onderstaande code netter:

items = []
item = input("Geef een lijst     - ")
while item != "":
    items.append(item)
    print("Breakpoint hier", items)
    item = input("Geef volgende item - ")

# De while list is nog niet aan bod gekomen, maar zullen we snel tegen gaan komen. De loop wordt hier niet een
# vast aantal keer herhaald, maar net zolang tot `item != ""` niet meer waar is. Met andere woorden, de gebruiker
# kan nieuwe items toevoegen tot die een lege regel geeft.

# Je kunt dit zien als een loop die in pseudo-code dit doet:

# if CONDITIE
# doe iets
# kijk opnieuw of de if waar is, dan herhaal
# anders ga door onder de loop

# Test om de ingevoerde lijst te tonen:
print(items)

# Code van de daadwerkelijke oefening (ter demonstratie)
for item in items:
    if len(item) == 4:
        print(item)
