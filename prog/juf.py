# Print de getallen 1 tot en met 100, maar elke keer dat een getal deelbaar is door 7 print je "juf" in plaats van het getal
# Een getal is deelbaar door 7 als `getal % 7` gelijk is aan 0
# Output: 1, 2, 3, 4, 5, 6, juf, 8, 9, 10, 11, 12, 13, juf, 15, 16, 17, 18, 19, 20

# Bonus: vervang ook getallen waar het cijfer 7 in zit:
# Output: 1, 2, 3, 4, 5, 6, juf, 8, 9, 10, 11, 12, 13, juf, 15, 16, juf, 18, 19, 20

# Hint: gebruik range, for _en_ if  =)

for getal in range(1, 101):
    if getal % 7 == 0:
        print("juf")
    else:
        print(getal)

# Bonus:

for getal in range(1, 101):
    if getal % 7 == 0 or '7' in str(getal):
        print("juf")
    else:
        print(getal)

# Op een regel:

for getal in range(1,101):
    if getal % 7 == 0 or '7' in str(getal):
        print("juf", end=" ")
    else:
        print(getal, end=" ")