print("Les 9 - Collections, Errors en Random")

# Database en album-collectie

albums = {
    "MarvinGaye1971": {"artist": "Marvin Gaye", "album": "What’s Going On", "year": 1971},
    "BeachBoys1966": {"artist": "The Beach Boys", "album": "Pet Sounds", "year": 1966},
    "JoniMitchell1971": {"artist": "Joni Mitchell", "album": "Blue", "year": 1971},
    "StevieWonder1976": {"artist": "Stevie Wonder", "album": "Songs in the Key of Life", "year": 1976},
    "Beatles1969": {"artist": "The Beatles", "album": "Abbey Road", "year": 1969},
    "Nirvana1991": {"artist": "Nirvana", "album": "Nevermind", "year": 1991},
    "FleetwoodMac1977": {"artist": "Fleetwood Mac", "album": "Rumours", "year": 1977},
    "Prince1984": {"artist": "Prince and the Revolution", "album": "Purple Rain", "year": 1984},
    "BobDylan1975": {"artist": "Bob Dylan, ‘", "album": "Blood on the Tracks", "year": 1975},
    "LaurynHill1998": {"artist": "Lauryn Hill", "album": "The Miseducation of Lauryn Hill", "year": 1998},
    "Beatles1966": {"artist": "The Beatles", "album": "Revolver", "year": 1966}
    }

my_collection = { "BruceSpringsteen1975", "MarvinGaye1971", "LaurynHill1998", "Beatles1966", "Beatles1969" }

# Oefeningen

# Hoe kan ik nu het beste een album uit de database aan mijn collectie toevoegen?
    # Schrijf een functie die een album toevoegt
    # Discussie uit de klas: het is netter om dit niet met een globale variabele te doen,
    # maar in een object (OOP) of met een functie die de aangepaste collectie teruggeeft
    # en het origineel zo laat. Voor dat eerste missen we de theorie nog (objecten), voor
    # dat tweede werkt Python tegen en moet je expliciet `my_collection` kopieren.

def add_album(tag):
    """
    Breidt de album-collectie uit door de tag in de verzameling albums toe te voegen. Als het album al in de collectie zat dan gebeurt er niets. Voert geen controle uit of het toe te voegen album in de database bekend is.

    Parameters:
        tag (str): Tag van het toe te voegen album.
    """

    my_collection.add(tag)

# Wat is het verschil tussen myBands1 en myBands2? Welke is in dit geval 'beter'?

def my_bands1(): # Als lijst, met dubbelen en volgorde
    """
    Geeft een lijst terug met alle album-titels uit my_collection.

    Returns:
        [str]: a list of album title strings
    """
    return [ albums[tag]["artist"] for tag in my_collection ]

def my_bands2(): # Als set, heb ik een artiest of niet? Volgorde onbelangrijk <<-- in dit geval beter
    """
    Geeft een set terug met alle album-titels uit my_collection.

    Returns:
        {str}: a set of album title strings
    """
    return { albums[tag]["artist"] for tag in my_collection }

# Voeg het album "tag" toe met de add_album functie hierboven. Roep nu een van beide my_bands aan. Wat gebeurt er?
    # Pas de functie add_album aan zodat deze een error geeft bij het toevoegen van een niet bestaande album-tag.

def add_album(tag):
    """
    Breidt de album-collectie uit door de tag in de verzameling albums toe te voegen. Als het album al in de collectie zat dan gebeurt er niets. Geeft een error als het toe te voegen album niet in de database bekend is.

    Parameters:
        tag (str): Tag van het toe te voegen album.
    """

    if tag in albums:
        my_collection.add(tag)
    else:
        raise ValueError("Dit album bestaat niet")  # <-- Nieuwe syntax (exceptions), een print is ook acceptabel.

# Maak een functie die true geeft als ik een album uit een gegeven jaar bezit

def bezit_album_jaar(jaar):
    """
    Kijk of my_collection een album uit een gegeven jaar bevat.

    Parameters:
        jaar (int): Het te zoeken jaar.

    Returns:
        bool: True als er een album uit jaar gevonden wordt, anders False
    """

    for tag in my_collection:
        if  albums[tag]["year"] == jaar:
            return True
    return False

# Print de namen van mijn albums, gesorteerd op jaar

def mijn_albums():
    """
    Print de namen van alle albums in de collectie, gesorteerd op jaar.
    """

    albs = []
    for tag in my_collection:
        albs.append( (albums[tag]["year"], albums[tag]["album"]) )
    albs.sort()
    for album in albs:
        print(album[1])

# Kies een random album uit mijn collectie om af te spelen

def random_owned_album():
    pass

# Kies een random album dat ik nog niet bezit om mijn waardebon te verzilveren

def random_unowned_album():
    pass

# Gegeven een naam van een artiest, geef een random album uit albums.
    # Als de artiest niet in de database bekend is, geef een error.

def random_album_by(artist):
    pass

# Exporteer een CSV met de gegevens van alle albums in mijn collectie

def export_collection():
    """
    Schrijf de naam, artiest en het jaar van elk album in de collectie naar een CSV-bestand.
    """

    with open("export.csv", "w") as csvfile: # w voor write, a voor append
        for tag in my_collection:
            album = albums[tag]
            row = f"{album['album']}; {album['artist']}; {album['year']}\n"
            csvfile.write(row)
    # Hier is de file gesloten (geen handmatige close() )

import csv # Dit soort dingen horen eigenlijk bovenaan

def export_with_writer():
    """
    Schrijf de naam, artiest en het jaar van elk album in de collectie naar een CSV-bestand, met behulp van een CSV Dictionary Writer.
    """

    with open("writer.csv", "w") as csvfile:
        fields = ["album", "artist", "year"]
        export_csv = csv.DictWriter(csvfile, delimiter=";", fieldnames=fields)
        for tag in my_collection:
            export_csv.writerow(albums[tag])

# Haal album-info op via een API en print alle key/values in het resultaat

import requests # Beter boven....
import json

def get_api(tag):
    """
    Haal een json-object voor een album op uit de API.

    Parameters:
        tag (str): De tag waarop gezocht moet worden.
    """

    API_KEY = 3098176 # Liever beter verstopt...
    resource_uri = f"http://album.peikos.net:62092/get/album?apikey={API_KEY}&tag={tag}"
    response = requests.get(resource_uri).json()
    for key in response:
        print(f"{key}: {response[key]}")

# Importeer een JSON file om de albumlijst uit te breiden

def load_json(filename):
    """
    Laad een JSON-bestand met albums in, en gebruik deze om de lokale "database" te updaten.

    Parameters:
        filename (str): Het pad van het te importeren JSON-bestand
    """

    with open(filename, "r") as json_file:
        albums.update(json.load(json_file))

# TODO Documenteer de functies met docstrings


print("Breakpoint hier")
