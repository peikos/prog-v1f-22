s = str()
woord = 'hogeschool utrecht'

for i in range(11, len(woord)):
    s = s + woord[i]

# range(11, len(woord)) = [11, 12, 13, 14, 15, 16, 17]

# Uitgeschreven:

for i in [11, 12, 13, 14, 15, 16, 17]:
    s = s + woord[i]

s = str()           # s = ""
s = s + woord[11]   # s = "u"
s = s + woord[12]   # s = "ut"
s = s + woord[13]   # s = "utr"
s = s + woord[14]   # s = "utre"
s = s + woord[15]   # s = "utrec"
s = s + woord[16]   # s = "utrech"
s = s + woord[17]   # s = "utrecht"

print(s)

s = str()
woord = 'hogeschool van amsterdam'

# Wat gebeurt er nu?

for i in range(11, len(woord)):
    s = s + woord[i]

print(s)

# De beginpositie staat vast, en is nu ook na 'hogeschool '. De eindpositie is verder:

print(range(11, len(woord)))

# Vraag 5

n = 0
for i in range(2, 9, 2):
    n = n + i

# range(2, 9) = [2, 3, 4, 5, 6, 7, 8]

# range(2, 9, 2) = [2, 4, 6, 8]

n = 0
for i in [2, 4, 6, 8]:
    n = n + i

# Uitgeschreven:

n = 0
n = n + 2
n = n + 4
n = n + 6
n = n + 8

# n = 0 + 2 + 4 + 6 + 8 = 20