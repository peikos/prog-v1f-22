# Handige links om Python te oefenen

- [CodeWars](https://www.codewars.com/join?language=python)
- [Exercism](https://exercism.org/tracks/python)
- [CodinGame](https://www.codingame.com/ide/puzzle/onboarding)

Daarnaast kun je de [Advent of Code](https://adventofcode.com/) doen voor leuke en uitdagende programmeeropdrachten. Deze zijn voor de iets meer gevorderden en geven geen Python-specifieke opdrachten of feedback, maar wel interessante problemen om over na te denken hoe je dit met programmeren op kan lossen. De eerste dagen zijn vaak redelijk te doen, maar het niveau wordt snel hoger, dus je kan in het begin misschien beter de eerste paar dagen van verschillende jaren doen dan proberen het laatste jaar helemaal af te maken. 1 December start er weer een nieuwe serie!

## Blok B
Ik gebruik dezelfde Git-repo voor het tweede blok voor AI / Steam, waar V1R nu ook aan toegevoegd wordt.

### Video - Git voor Steam
[![YouTube Link Git video](http://img.youtube.com/vi/n6zXP8UwwbY/0.jpg)](https://youtu.be/n6zXP8UwwbY)

### AI Les 2 - Bewerkingen

[Grafiek van de behandelde functies en de omgekeerden](https://www.desmos.com/calculator/qug5psscqe). Je kan door op de bolletjes de klikken lijnen aan en uitzetten. De zwarte stippellijn is de "spiegel". De functies en hun omgekeerde zijn steeds in dezelfde kleur, met de omgekeerde met streepjes. Met de slider b kun je deze parameter aanpassen. Met $b=2$ heb je het gewone kwadraat en de gewone wortel.

[WolframAlpha](https://www.wolframalpha.com/input?key=&i=log_4%2816%29) is een wat meer geavanceerde calculator om met bijvoorbeeld logaritmen te werken. Als je gewoon "4 log 16" in Google gooit, krijg je $4 \cdot log_{10}(16)$. Wolfram Alpha gebruikt de notatie `log_4(16)` om $log_4(16)$ uit te rekenen.

### AI Les 7 - Statistiek
- [Voorbeeld Frequentietabel](https://docs.google.com/spreadsheets/d/1EqsNdqHNjaZAp_l1KU9DJZGnEsGSM7mANLDWWw6k6DM/edit?usp=sharing)
