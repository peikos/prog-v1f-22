import requests
import os

def open_dl_if_missing(local, remote, binary=False):
    """Opent een bestand, na het eerst te downloaden als dat nodig is (en lukt). Werkt op tekstbestanden en binaire bestanden, maar je moet wel aangeven welke je verwacht. Geeft een file object terug net als `open()`, en kan dus gebruikt worden in een `with`/`as` blok:


    Voorbeeld:
        with open_dl_if_missing("steam.json", "https://raw.githubusercontent.com/tijmenjoppe/AnalyticalSkills-student/master/project/data/steam.json") as file:
            print(file.read())


    Bronvermelding:
        Als je deze code gebruikt in je Steam Project, verwijs dan in je bronvermelding bij de functie de URL https://gitlab.com/peikos/prog-v1f-22 en/of auteur Brian van der Bijl.


    Parameters:
        local(str):   Lokaal pad - open dit bestand of/en sla een download hier op.
        remote(str):  Remote pad - hier wordt het bestand vandaan gedownload.
        binary(bool): False voor tekstuele bestanden, True voor e.g. plaatjes of PDF

    Return-waarde:
        read only file-object, zoals `open(local, "r")` of `open(local, "rb")`.


    Fout-condities:
        Er zijn meerdere mogelijke exceptions voor deze functie. De belangrijkste categorien zijn:

        IOError:            Er is iets misgegaan met het openen van het lokale bestand, waarschijnlijk bij het schrijven van de data.
        RequestException    Er is iets misgegaan bij het downloaden, mogelijk een verbindingsfout of een vekeerde URL.
    """
    try:
        mode = "rb" if binary else "r"
        if os.path.exists(local):
            return open(local, mode)
        else:
            wmode = "wb" if binary else "w"
            request = requests.get(remote)
            with open(local,wmode) as file:
                if binary:
                    file.write(request.content)
                else:
                    file.write(request.text)
                return open(local, mode)

    except requests.RequestException as err:
        raise requests.RequestException("Fout in het binnenhalen van de file!\n\n" + str(err)) from None

    except Exception as err:
        raise IOError("Fout in het openen van de lokale file\n\n" + str(err)) from None

with open_dl_if_missing("README.md", "https://raw.githubusercontent.com/tijmenjoppe/AnalyticalSkills-student/master/README.md") as file:
    print(file.read())

open_dl_if_missing("data.json", "https://raw.githubusercontent.com/tijmenjoppe/AnalyticalSkills-student/tree/master/project/data/steam.json")
open_dl_if_missing("tijmen.jpg", "https://avatars.githubusercontent.com/u/15344757?v=4", binary=True)
open_dl_if_missing("hl15.jpg", "https://lab.peikos.net/images/hl15.jpg", binary=True)
