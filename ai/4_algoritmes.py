def divisors(n):
    """Dit is de naieve versie waar je mee zou kunnen beginnen.
    Het nadeel is dat deze oplossing vrij langzaam is --- of beter gezegd: het kan sneller...
        """
    divs = []
    for i in range(1,n+1):
        if n%i == 0:
            divs.append(i)
    return divs

def divisors1(n):
    """In de les over delers hebben we gezien dat delers altijd in paren voorkomen.
    Voor iedere gevonden deler kunnen we meteen de mirror-universe counterpart n/deler noteren.
    Op deze manier hoeven we niet verder te zoeken dan de wortel van het getal waar we de delers zoeken: voor elk getal onder deze grens zit de counterpart boven de grens.
    Een edge-case zit hem in de getallen die een perfect kwadraat zijn, zoals 144. Als we 12 toevoegen is 144/12=12 niet nog een keer nodig.
    Deze functie is een aanpassing op de bovenstaande, met een toegevoegde break als we klaar zijn.
    """
    divs = []
    for i in range(1,n+1):
        if n%i == 0:
            divs.append(i)
            if i * i != n:             # Deler heeft een counterpart?
                divs.append(n//i)
        if i*i > n:                    # We zijn klaar
            break
    return divs

import math

def divisors2(n):
    """
    In het vorige voorbeeld gebruiken we een break om onze loop vroegtijdig af te breken.
    Omdat we van te voren al weten tot waar de loop moet lopen is het netter om de range meteen aan te passen:
    """
    divs = []
    root = math.ceil(math.sqrt(n))
    for i in range(1, root + 1):
        if n%i == 0:
            divs.append(i)
            if i != root:
                    divs.append(n//i)
    return divs

# De divisors staan met de optimalisaties niet meer op volgorde. Als we dat willen moeten we het rusultaat nog sorteren:

print(sorted(divisors(60)))
print(sorted(divisors1(60)))
print(sorted(divisors2(60)))

print(sorted(divisors1(145235151)))
print(sorted(divisors2(145235151)))
