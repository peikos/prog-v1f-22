def swap(lst, idx_a, idx_b):
    """Pas lst aan door item a en b te verwisselen"""
    a = lst[idx_a]
    lst[idx_a] = lst[idx_b]
    lst[idx_b] = a

def swap(lst, idx_a, idx_b):
    """Kortere notatie"""
    lst[idx_a], lst[idx_b] = lst[idx_b], lst[idx_a]

foo = [1,2,3,4]
print("Before swap", foo)
swap(foo,0,3)
print("After swap", foo) # [4,2,3,1]

print()

def find_idx_of_minimum(lst):
    """Geef de index terug van het kleinste item"""
    min_val = lst[0]
    min_idx = 0
    for idx, item in enumerate(lst):
        if item < min_val:
            min_val = item
            min_idx = idx
    return min_idx

bar = [4,6,1,3,7]
print(f"Minimum of {bar} is {bar[find_idx_of_minimum(bar)]} at index {find_idx_of_minimum(bar)}")

print()

def selection_sort(lst):
    """Combineer index-zoeker en swap functies om een lijst (in-place) te sorteren"""
    for start_unsorted in range(len(lst)):
        min_idx = start_unsorted + find_idx_of_minimum(lst[start_unsorted:])
        print(f"Swapping index {start_unsorted} and index {min_idx} (value {lst[min_idx]})")
        swap(lst, min_idx, start_unsorted)

test = [1,6,2,7,3,5]
print("Before: ", test)
selection_sort(test)
print("After: ", test)
