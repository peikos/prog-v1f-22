def plus(a, b):
    """Plus is de basis, dit moet een persoon of computer kunnen om de rest daarop te bouwen"""
    return a + b

def negatief(a):
    """Ieder getal heeft een negatieve, waarmee we het getal 'omkeren'. Deze wordt met als -x geschreven, maar dat is anders dan x - y voor aftrekken. De negatieve is uniek, een getal heeft nooit twee verschillende negatieven. 0 is de negatieve van zichzelf."""
    return -a

def minus(a, b): # a - b
    """Aftrekken van twee getallen kan met optellen en de negatieve. Tel het eerste getal op bij de negatieve van de tweede. Omdat specifiek het tweede getal omgekeerde wordt is a-b niet hetzelfde als b-a (tenzij a en b hetzelfde zijn)."""
    return plus(a, negatief(b))

def keer(a, b): # a * b
    """Normaal geschreven als a·b, maar ook als a⨯b of gewoon ab. Keer kunnen we uitdrukken met herhaaldelijk optellen. Bij a keer b tellen we a kopien van b bij elkaar op, of b kopieen van a (de volgorde maakt weer niet uit, a*b is b*a."""
    totaal = 0

    for _ in range(a):
        totaal += b

    return totaal

def omgekeerde(a): # 1 / a
    """Net als met de negatieve kunnen we voor (vrijwel) ieder getal nog een omgekeerde vaststellen, die ons gaat helpen om delen te versimpelen. Wederom is de omgekeerde uniek, en is de omgekeerde van de omgekeerde het getal zelf: de omgekeerde van 2 is 1/2 (0.5), en de omgekeerde van 1/2 is 2. In dit geval is 1 de omgekeerde van zichzelf, en heeft 0 geen omgekeerde (oftewel: we kunnen niet delen door 0).

                  1
    In breukvorm: -
                  a """
    return 1 / a

def gedeeld_door(a, b):
    """Delen door is hetzelfde trucje als aftrekken, maar nu met `keer` en `omgekeerde` in plaats van `plus` en `negatief`."""
    return(keer(a, omgekeerde(b)))

def macht(a, b):
    """De macht is herhaaldelijk vermenigvuldigen, net als vermenigvuldigen herhaaldelijk optellen is. Hier maakt de volgorde wel uit: a tot de macht b is (op een toevallige uitzondering) iets anders dan b tot de macht a. Vaak schrijven we op de computer a^b omdat de wiskundige notatie niet op het toetsenbord zit (net als * voor keer). De macht functie werkt voor natuurlijke getallen b; negatieve machten en gebroken machten die we in de les hebben gezien zijn zo gedefinieerd dat ze kloppen met deze definitie, maar kunnen we niet meer met een for-loop programmeren (negatieve of halve for-loops zijn niet een ding)."""
    totaal = 1

    for _ in range(b):
        totaal *= a

    return totaal

# Test de laatste functie.
print(macht(2, 9)) # 512




# EXTRA (nav vraag in V1F)

# We kunnen breuken weergeven met (tuples van) twee getallen. Hier moeten we dan wel speciale versies van de functies hierboven schrijven om met de tuples om te gaan. Ook moeten we kunnen versimpelen en weten dat twee breuken hetzelfde kunnen zijn als de getallen anders zijn, maar ze tot dezelfde breuk versimpelen:
tweederde = (2,3)
tweederde_ook = (4,6)

# Voorbeeld voor optellen zou er dan zo uit zien. De implementatie zit in de FA, dus is niet gegeven. In de comments de waardes voor een specifieke aanroep met getallen uit de Socrative.
drie_elfde = (3, 11)
def plus_breuk(a, b): # a = tweederde, b = drie_elfde
    a[0] # 2
    a[1] # 3
    b[0] # 3
    b[1] # 11
    # TODO
    return (31, 33)
