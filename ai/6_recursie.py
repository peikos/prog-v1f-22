# De mystery functie van de slides
print("# Mystery functie slides")

def functie(x, y):
    if x == 1:
        return y
    else:
        return y + functie(x-1, y)

# Annotated versie, om te zien wat er gebeurt
def functie(x, y):
    print(f"Functie op {x} en {y}.")
    if x == 1:
        print(f"Klaar! Ik geef {y} terug.")
        return y
    else:
        print(f"x = {x} > 1. Recursie!")
        antwoord = functie(x-1, y)
        print(f"Terug uit recursieve aanroep, antwoord was {antwoord} dus mijn antwoord wordt {y} + {antwoord} = {y + antwoord}.")
        return y + antwoord

print(functie(3,4))

print()
print()

print("# Even")

# Even functie d.m.v. recursie. We hebben hier twee base cases, 0 en 1

def even(n):
    """Geef terug of een getal even is. Werkt alleen voor n >= 0 (anders bewegen we van de base case af in plaats van ernaartoe).

    Parameters:
        n(int): een natuurlijk getal, oftewel int en >= 0

    Return-waarde:
        bool: True als n even is, False als n oneven is.
    """
    if n == 0:         # Base case voor even getallen
        return True
    if n == 1:
        return False   # Base case voor oneven getallen
    return even(n-2)   # Recursieve aanroep. In essentie bootsen we hier % na.

print(even(2))
print(even(13))
print(even(42))

print()
print()

print("# Faculteit")

# Faculteit iteratief (met loops) en recursief.

def faculteit_loop(n):          # Voorbeeld: n = 12
    totaal = 1
    for getal in range(1, n+1): # Range is 1 2 3 4 5 6 7 8 9 10 11 12
        print(f"{totaal} * {getal} = {totaal*getal}")
        totaal *= getal
    return totaal


print(faculteit_loop(5))

# Recursieve versie, plain.

def faculteit_rec(n):
    if n == 0:
        return 1
    else:
        return n * faculteit_rec(n - 1)

# En nu met extra prints zodat de executie beter te volgen is.

def faculteit_rec(n):
    if n == 0:
        return 1
    else:
        print(f"{' '*(10-n)}{n} * faculteit_rec({n-1})")
        rec = faculteit_rec(n - 1)
        print(f"{' '*(10-n)}{n} * {rec} = {n*rec}")
        return n * rec


print(faculteit_rec(5))

print()
print()

print("# Merge Sort")

# Merge sort met dubbele recursie. Deze versie geeft een gesorteerde kopie terug. Deze sort werkt voor getallen, strings, of andere types met een ordering.

def merge(lst_a, lst_b):
    """Merge twee gesorteerde lijsten tot een nieuwe gesorteerde lijst.

    Parameters:
        lst_a(list): een gesorteerde sublijst
        lst_b(list): een gesorteerde sublijst

    Return-waarde:
        list: een samengevoegde lijst, gesorteerd

    """

    merge = []  # Hier komen alle elementen in te staan.
    print("Merging", lst_a, "&", lst_b)

    # Blijf mergen tot een van beide lijsten leeg is.
    while lst_a != [] and lst_b != []:

        # Als de linkerlijst een kleinere eerste waarde heeft, pak die.
        # Zo niet, pak de andere.
        if lst_a[0] <= lst_b[0]:
            merge.append(lst_a.pop(0))
        else:
            merge.append(lst_b.pop(0))

    # While loop is klaar, een van beide lijsten is op. Plak de andere in
    # een keer achterop de gesorteerde lijst.
    if lst_a == []:
        merge.extend(lst_b)
    elif lst_b == []:
        merge.extend(lst_a)

    # Print en return de lijst.
    print("Merged into", merge)
    return merge

def merge_sort(lst):
    """Sorteer een lijst met het merge-sort algoritme.

    Parameters:
        lst(list): een lijst met sorteerbare items.

    Return-waarde:
        list: een gesorteerd lijst.

    """

    # Base case voor recursie: als de lijst een element heeft, dan is deze
    # per definitie gesorteerd.
    if len(lst) == 1:
        return lst

    # Zo niet, splits de lijst halverwege in twee sublijsten.
    mid = len(lst) // 2
    lst_a = lst[:mid] # Van begin tot aan index mid
    lst_b = lst[mid:] # Van index mid tot aan eind

    print("Split", lst, "into", lst_a, "&", lst_b)

    # Recursieve aanroep op beide sublijsten.
    a_sorted = merge_sort(lst_a)
    b_sorted = merge_sort(lst_b)

    # De twee sublijsten zijn nu gesorteerd, gebruik merge om samen te voegen.
    return merge(a_sorted, b_sorted)


list = [1,6,2,8,3,6,9,2]
sorted = merge_sort([1,6,2,8,3,6,9,2])
print(list)
print(sorted)

