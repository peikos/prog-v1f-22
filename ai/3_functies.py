import matplotlib.pyplot as plt


def kwadraat(): # x^2
    xs = [x / 1000 - 5.2 for x in range(10400)]
    ys = [x**2 for x in xs]

    plt.plot(xs, ys)
    plt.show()


def kwadraat_lofi(): # x^2 met minder punten
    xs = [x - 5 for x in range(11)]
    ys = [x**2 for x in xs]

    plt.plot(xs, ys)
    plt.show()


def exponent(): # 2^x
    xs = [x / 1000 - 5.2 for x in range(10400)]
    ys = [2**x for x in xs]

    plt.plot(xs, ys)
    plt.show()


def wortel(): # x^2 omgekeerd
    xs = [x / 1000 for x in range(5200)]   # Kleinere input-range!
    ys = [x**2 for x in xs]

    plt.plot(xs, ys)
    plt.plot(ys, xs)
    plt.show()


def log2(): # 2^x omgekeerd
    xs = [x / 1000 - 5.2 for x in range(10400)]
    ys = [2**x for x in xs]

    plt.plot(xs, ys)
    plt.plot(ys, xs)
    plt.show()


log2()
