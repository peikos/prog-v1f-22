import os

if os.getenv("API_KEY"):
    API_KEY=os.getenv("API_KEY")
else:
    print("Hey, pannenkoek! Je moet je API_KEY nog instellen!")
    exit(1)

# Dit werkt zodra je een variabele API_KEY in PyCharm hebt aangemaakt.
print(API_KEY)
